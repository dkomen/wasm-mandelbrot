extern crate num;
use num::complex::Complex as complex;
use wasm_bindgen::prelude::*;

const WIDTH: usize = 2500;
const HEIGHT: usize = 1600;
const OUTPUT_BUFFER_SIZE: usize = (WIDTH * HEIGHT) * 4;
static mut OUTPUT_BUFFER: [u8; OUTPUT_BUFFER_SIZE] = [0; OUTPUT_BUFFER_SIZE];

#[wasm_bindgen]
pub fn get_output_buffer_pointer() -> *const u8 {
  let pointer: *const u8;
  unsafe {
    pointer = OUTPUT_BUFFER.as_ptr();
  }

  return pointer;
}

#[wasm_bindgen]
pub fn start_generating(max_iterations: i32) {
    start(max_iterations);
}

fn start(max_iterations: i32) {
    // #[cfg(debug_assertions)]
    // console_error_panic_hook::set_once();

    const RE_START: f32 = -2.0;
    const RE_END: f32 = 1.0;
    const IM_START: f32 = -1.0;
    const IM_END: f32 = 1.0;

    let mut value = num::complex::Complex::new(0.0, 0.0);
    let mut mandelbrot_result: f32 = 0_f32;
    let mut pixel_color: u8 = 0;

    for y in 0..HEIGHT {
        for x in 0..WIDTH {
            value = complex::new(RE_START + (x as f32 / WIDTH as f32) * (RE_END - RE_START), IM_START + (y as f32 / HEIGHT as f32) * (IM_END - IM_START));
            mandelbrot_result = get_mandelbrot(&max_iterations, value);            
            pixel_color = (255_f32 - (mandelbrot_result * 255_f32 / max_iterations as f32)) as u8;
            let pixel: usize = x + (WIDTH as usize * y);
            let pixel_rgba_index: usize = pixel*4;
            unsafe { // Must be unsafe because we are dealing with externally shared memory
                OUTPUT_BUFFER[pixel_rgba_index] = pixel_color;
                OUTPUT_BUFFER[pixel_rgba_index + 1] = pixel_color;
                OUTPUT_BUFFER[pixel_rgba_index + 2] = pixel_color;
                OUTPUT_BUFFER[pixel_rgba_index + 3] = 255;
            }
        }
    };
}

/// Do mandelbrot calculation
fn get_mandelbrot(max_iterations: &i32, value: complex<f32>) -> f32 {
    let mut n: f32 = 0_f32;
    let mut z: complex<f32> = complex::new(0.0, 0.0);
    while get_absolute_of_complex(z) <= 2.0 && n < *max_iterations as f32 {
        z = (z * z) + value;
        n += 1_f32;
    }
    n
}

/// Get the abolute value of a complex number
fn get_absolute_of_complex(value: complex<f32>) -> f32 {
    ((value.re * value.re) + (value.im * value.im)).sqrt()
}
