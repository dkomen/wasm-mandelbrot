import wasmInit from "./pkg/graphics.js";
const MAX_ITERATIONS = 275;
const WIDTH = 2500;
const HEIGHT = 1600;
const canvasElement = document.getElementById("fractal");
const canvasContext = canvasElement.getContext("2d");
const canvasImageData = canvasContext.createImageData(WIDTH, HEIGHT);



//==== RUST WASM ===============================================
const runWasm = async (max_iterations) => {
  // Instantiate our wasm module
  const rustWasm = await wasmInit("./pkg/graphics_bg.wasm");
  const ShowRewsults = () => {
    rustWasm.start_generating(max_iterations);

    const wasmByteMemoryArray = new Uint8Array(rustWasm.memory.buffer);
    const outputPointer = rustWasm.get_output_buffer_pointer();
    const imageDataArrayWasm = wasmByteMemoryArray.slice(outputPointer, outputPointer + WIDTH * HEIGHT * 4);
    canvasImageData.data.set(imageDataArrayWasm);
    canvasContext.putImageData(canvasImageData, 0, 0);
    may_continue = true;
  };
  
  ShowRewsults();  
};


runWasm(MAX_ITERATIONS)