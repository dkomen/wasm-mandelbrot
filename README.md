# Mandelbrot with WebAssembly (Rust)

## Rust WebAssembly to plot Mandelbrot fractal

- Compile with `wasm-pack build --target web`
- If you have python installed (on linux you propably do) start a server in root folder with `python -m SimpleHTTPServer 8000` else run `npm start` for Npm.
